package helperclasses;

public class PageObjects {

    public static String login = "//*[@id='cms-header']/div/div[1]/div/a[2]";
    public static String searchid = "lst-ib";
    public static String text = "l";
    public static  String email = "//*[@id=\'lmn_login_widget_layout\']/div/div[4]/div/input";
    public static  String password = "//*[@id=\'lmn_login_widget_layout\']/div/div[5]/div/input";

    public static String wemail = "//*[@id=\'lmn_login_widget_modal\']/div/div/div[2]/div[4]/div/input";
    public static  String wpassword = "//*[@id='lmn_login_widget_modal']/div/div/div[2]/div[5]/div/input";

    public static String wsignin ="//*[@id=\'lmn_login_widget_modal\']/div/div/div[2]/div[7]/button[1]";

    public static  String Errorlocator = "//*[@id='lmn_login_widget_modal']/div/div/div[2]/div[3]/div";
    public static String ExpectedM = "The combination of your email and password do not match";

    public static String leavingfrom = "flights-search-from";
    public static String arriveat = "flights-search-to";

    public static String search= "//*[@id=\'flights-cannonball\']/form/div[2]/fieldset[3]/div[1]/div[3]/button";
    public static String outbounddate = "flights-check-in";
    public static String returndate ="flights-check-out";
    public static String monthheader ="headingText";
    public static String clicker = "//*[@id=\'flights-cannonball\']/form/div[2]/fieldset[3]/div[2]/div/div/div[3]/div[2]/div/div[1]/div[1]";
public static String monthcontainer = "monthInnerContainer";
public static String dayitems = "cellOuter";
public static String Flights = "flights-tab-link";
}
