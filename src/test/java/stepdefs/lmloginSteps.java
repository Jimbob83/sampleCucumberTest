package stepdefs;

import Utils.Waits;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import helperclasses.AccountActions;
import Utils.CommonFunctions;

/**
 * Created by jamesm on 17/08/2018.
 */
public class lmloginSteps extends  CommonFunctions {
    AccountActions AA = new AccountActions();

    @Given("^I am on  \"([^\"]*)\"$")
    public void iAmOn(String url) throws Throwable {

        goTo(url);
    }

    @And("^I select \"([^\"]*)\"$")
    public void iSelect(String element) throws Throwable {
        ClickOnMaster(element);
    }
    @And("^I know which page Im directed to$")
    public void iKnowWhichPageImDirectedTo() throws Throwable {
        surveryHandler();
    }


    @And("^I enter \"([^\"]*)\" in \"([^\"]*)\"$")
    public void iEnterIn(String input, String locator) throws Throwable {
        enterdetails(input, locator);
    }


    @Then("^I should be in my \"([^\"]*)\"$")
    public void iShouldBeInMy(String account) throws Throwable {
        assertTitle(account);// Write code here that turns the phrase above into concrete actions

    }

    @And("^I can select select my account$")
    public void iCanSelectSelectMyAccount() throws Throwable {
       AA.openprofile();
    }



    @Then("^\"([^\"]*)\" \"([^\"]*)\" is present$")
    public void isPresent(String type, String locator) throws Throwable {
    getMessage(type, locator);

    }


    @And("^I select \"([^\"]*)\" \"([^\"]*)\" as \"([^\"]*)\"$")
    public void iSelectAs(String day   , String month, String locator) throws Throwable {
        DateMaster(day, month, locator);
    }

    @And("^I click \"([^\"]*)\"$")
    public void iClick(String Fights) throws Throwable {
       ClickOnMaster(Fights); // Write code here that turns the phrase above into concrete actions

    }
}








