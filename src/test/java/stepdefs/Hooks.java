package stepdefs;


import Utils.CommonFunctions;
import Utils.Drivers;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hooks extends CommonFunctions {

    @Before
    public void setup() {

        if (driver == null) {
            driver = new Drivers().driver();
//            driver.manage().window().maximize();
//            driver.manage().window().fullscreen();
        }else {
            driver.manage().deleteAllCookies();
        System.out.println("cookies deleted");
//        driver.manage().window().fullscreen();
        }
    }

    @After
    public void tearDown() {
        driver.manage().deleteAllCookies();
//        driver.close();
//      driver.quit();
        driver = null;
    }
}
