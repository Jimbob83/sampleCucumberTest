package stepdefs;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import helperclasses.EjetCommonFunctions;

public class ebaySteps {

    EjetCommonFunctions ecf = new EjetCommonFunctions();

    @Given("^User is \"([^\"]*)\"$")
    public void user_is(String url) throws Throwable {
        ecf.goTo(url);// Write code here that turns the phrase above into concrete actions

    }

    @And("^User enters camera in search field$")
    public void user_enters_camera_in_search_field() throws Throwable {
        ecf.ebaysearch();
    }

    @Then("^User is presented with capris$")
    public void user_is_presented_with_cameras() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

}
