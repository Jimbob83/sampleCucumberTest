package stepdefs;

import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import helperclasses.EjetCommonFunctions;

public class ejsearchSteps {

EjetCommonFunctions ecf = new EjetCommonFunctions();

    @Given("^User is on \"([^\"]*)\"$")
    public void user_is_on(String url) throws Throwable {
        ecf.goTo(url);
    }

    @And("^User selects Depature$")
    public void user_selects_Depature() throws Throwable {
        ecf.departure();
    }


    @And("^User selects Arrival$")
    public void user_selects_Arrival() throws Throwable {
        ecf.arrival();
    }

    @And("^User selects outbound date$")
    public void user_selects_outbound_date() throws Throwable {
        ecf.outdate();

    }

    @Given("^User selects return date$")
    public void user_selects_return_date() throws Throwable {
       ecf.returndate();
    }

    @Given("^User selects search$")
    public void user_selects_search() throws Throwable {
        ecf.search();
    }

    @Then("^User is presented with flights available$")
    public void user_is_presented_with_flights_available() throws Throwable {
        ecf.results();
    }



}
