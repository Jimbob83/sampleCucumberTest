package stepdefs;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import Utils.CommonFunctions;

/**
 * Created by jamesm on 15/08/2018.
 */
public class SampleTestSteps {

    CommonFunctions cmf = new CommonFunctions();

    @Given("^i navigate to (.+)$")
    public void i_navigate_to(String url){
        cmf.goTo(url);
    }

    @Then("^i should be presented with (.+) site$")
    public void i_should_be_presented_with_website(String site) {
        cmf.assertTitle(site);
    }
}
