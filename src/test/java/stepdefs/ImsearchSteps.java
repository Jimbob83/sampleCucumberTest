package stepdefs;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import Utils.CommonFunctions;

/**
 * Created by jamesm on 17/08/2018.
 */
public class ImsearchSteps extends CommonFunctions {

    @Given("^I am on \"([^\"]*)\"$")
    public void iAmOn(String url) throws Throwable {
        goTo(url);
    }

    @And("^I type \"([^\"]*)\" in \"([^\"]*)\"$")
    public void iTypeIn(String value, String locator) throws Throwable {
        // Write code here that turns the phrase above into concrete actions

        inputField(locator, value);
    }

    @Given("^I select last minute site from search results$")
    public void i_select_last_minute_site_from_search_results() throws Throwable {
        selectlastminute();

    }

    @Then("^i should be presented with \"([^\"]*)\"$")
    public void iShouldBePresentedWith(String value)
    {assertoutcome(value);

    }


}
