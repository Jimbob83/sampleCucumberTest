package Utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Drivers {

    public static WebDriver driver() {
       System.setProperty(ChromeDriverService.CHROME_DRIVER_EXE_PROPERTY, System.getProperty("user.dir") + "/src/test/resources/Drivers/chromedriver");
//        System.setProperty(ChromeDriverService.CHROME_DRIVER_EXE_PROPERTY, System.getProperty("C:\\Users\\James\\Documents\\Tester\\chromedriver.exe");
       WebDriver driver = new ChromeDriver();

//System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir") + "/src/test/resources/drivers/geckodriver");
//
//                WebDriver driver = new FirefoxDriver();
    return driver;
    }
}
