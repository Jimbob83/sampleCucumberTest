package Utils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

import static Utils.CommonFunctions.driver;

public class Waits {


    public void WaitForElementToBeClickable(WebDriver driver, String id, boolean xpath) {
        WebDriverWait driverWait = new WebDriverWait(driver, 20);
        if (xpath) {
            driverWait.until(ExpectedConditions.elementToBeClickable(By.xpath(id)));
        } else {
            driverWait.until(ExpectedConditions.elementToBeClickable(By.id(id)));
        }
    }

    public void WaitforElementToBePresent(WebDriver driver, String id, boolean xpath) {
        WebDriverWait driverWait = new WebDriverWait(driver, 20);
        if (xpath) {
            driverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(id)));
        } else {
            driverWait.until(ExpectedConditions.presenceOfElementLocated(By.id(id)));
        }
    }


    public void implicitWait() {
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }
}