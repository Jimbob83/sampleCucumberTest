package Utils;

import helperclasses.PageObjects;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.ServerSocket;
import java.util.List;

/**
 * Created by jamesm on 17/08/2018.
 */
public class CommonFunctions {
    PageObjects po = new PageObjects();
    Waits waits = new Waits();

    public static WebDriver driver = new Drivers().driver();


    public void ClickOnServant(String element, boolean xpath) {
//        waits.WaitForElementToBeClickable(driver, element, xpath);
        if (xpath) {
            driver.findElement(By.xpath(element)).click();
        } else {
            driver.findElement(By.id(element)).click();
        }
    }

    public void TypeThis(String element, String value, boolean xpath) {
        System.out.println(element);


        if (xpath) {
            driver.findElement(By.xpath(element)).sendKeys(value);
        } else {
            driver.findElement(By.id(element)).sendKeys(value);
            driver.findElement(By.id(element)).sendKeys(Keys.DOWN);

//            driver.findElement(By.id(element)).sendKeys(Keys.ARROW_DOWN,Keys.ENTER);
        }


    }

    public void goTo(String url) {
        driver.get(url);
    }

    public void getTitle(String site) {
        driver.getTitle();
    }

    //public String TheTitleIs(site){
//        System.out.println(getTitle());
//}
    public void assertTitle(String pageName) {
        switch (pageName) {
            case "gmail":
                Assert.assertEquals("Gmail - Free Storage and Email from Google", driver.getTitle());
                break;
            case "google":
                Assert.assertEquals("Google", driver.getTitle());
                break;
            case "account":
                System.out.println(driver.getTitle());
                Assert.assertEquals("Book Cheap Holidays, Flights, Hotels & City Breaks | lastminute.com™", driver.getTitle());
                break;
            case "https://www.lastminute.com":
                Assert.assertEquals("Book Cheap Holidays, Flights, Hotels & City Breaks | lastminute.com™",
                        driver.getTitle());
                break;
            default:
                throw new RuntimeException(pageName + " NOT FOUND");
        }
    }

    public void assertoutcome(String value) {
        switch (value) {
            case "results":
                String Results = driver.findElement(By.xpath("//*[@id='app-container']/div/div/div/div/div/div[5]/div[1]/div/div[1]/div[2]/div[1]/div/div/span/strong")).getText();
Assert.assertTrue(Results.contains(value));
//                Assert.assertEquals("116 results",

                break;

            default:
                throw new RuntimeException(value + " NOT FOUND");
        }
    }

    public void inputField(String locator, String value) {
        switch (locator) {
            case "search field":
                locator = po.searchid;

                TypeThis(locator, value, false);
                break;
        }
    }

    public void selectlastminute() {
        driver.findElement(By.id("vn1s0p1c0")).click();
    }


    public void ClickOnMaster(String locator) {
        switch (locator) {
            case "login":
                ClickOnServant(po.login, true);
                break;
            case "signin":
                ClickOnServant(po.wsignin, true);
                break;
            case "Search":
                ClickOnServant(po.search, true);
                break;
            case "Flights":
                ClickOnServant(po.Flights, false);
                break;
            default:
                throw new RuntimeException(" NOT FOUND");
        }
    }

    public void DateMaster(String day, String month, String locator) {

        switch (locator) {
            case "outbounddate":
                // waits.implicitWait();
                // surveryHandler();
                ClickOnServant(po.outbounddate, false);
                finddate(day, month);
                break;
            case "returndate":
                // waits.implicitWait();
                // surveryHandler();
                ClickOnServant(po.returndate, false);
//                waits.WaitForElementToBeClickable(driver, "///*[@id'dp-cannonball']/form/div[1]/fieldset[3]/div[2]/div/div/div[3]/div[1]/div/div[1]/div[2]", true);
                finddate(day, month);
                break;
            default:
                throw new RuntimeException(" NOT FOUND");
        }
    }

    public void finddate(String day, String month) {
        String c = driver.findElement(By.className(po.monthheader)).getText();

        while (!driver.findElement(By.className(po.monthheader)).getText().equals(month)) {
            driver.findElement(By.xpath(po.clicker)).click();
        }
        System.out.println(c);
        WebElement dateWidgetFrom = driver.findElement(By.className(po.monthcontainer));

        //This are the rows of the from date picker table
        List<WebElement> days = dateWidgetFrom.findElements(By.className(po.dayitems));


        //This are the columns of the from date picker table
        for (WebElement x : days) {
            if (x.getText().equals(day)) {
                x.click();

                break;
            }
        }

//}
//
////    public void alertBoo() {
////        WebDriverWait wait = new WebDriverWait(driver, 30);
////        try {
////            driver.switchTo().parentFrame();
////        } catch (TimeoutException tOut) {
////            System.out.println("Alert Not Present");
////        }
////
////    }

//    public void loginHandler() {
//        surveryHandler();
//        WhichPage(input, locator);
//    }
    }

    public boolean whichPage() {
        int i = driver.findElements(By.name("email")).size();
        boolean x;
        if (i > 0) {
            x = true;
        } else {
            x = false;
        }
        return x;

    }


    public void enterdetails(String input, String locator) throws InterruptedException {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        switch (locator) {


            case "emailfield":


                if (whichPage()) {
                    locator = po.wemail;
                    TypeThis(locator, input, true);
                } else {
                    locator = po.email;
                    TypeThis(locator, input, true);

                }
                break;
            case "passwordfield":


                if (whichPage()) {
                    locator = po.wpassword;
                    TypeThis(locator, input, true);
                } else {
                    locator = po.password;
                    TypeThis(locator, input, true);

                }
                break;
            case "departure":
                waits.implicitWait();
                surveryHandler();

                driver.findElement(By.id(po.leavingfrom)).sendKeys(input);
                Thread.sleep(1000);
                driver.findElement(By.id(po.leavingfrom)).sendKeys(Keys.DOWN);

                //JavascriptExecutor js = (JavascriptExecutor) driver;
                String script = "return document.getElementById(\"flights-search-from\").value;";
                String text = (String) js.executeScript(script);
                while (!text.equalsIgnoreCase("Newcastle (NCL) , United Kingdom")) {
                    driver.findElement(By.id(po.leavingfrom)).sendKeys(Keys.DOWN);

                    text = (String) js.executeScript(script);
                    System.out.println(text);
                }
                driver.findElement(By.id(po.leavingfrom)).sendKeys(Keys.TAB);
                Thread.sleep(1000);

                break;

            case "arrival":
                //waits.implicitWait();
                // surveryHandler();
                driver.findElement(By.id(po.arriveat)).click();
                driver.findElement(By.id(po.arriveat)).sendKeys(input);
                Thread.sleep(1000);
                driver.findElement(By.id(po.arriveat)).sendKeys(Keys.DOWN);

                //    JavascriptExecutor js2 = (JavascriptExecutor) driver;
                String script2 = "return document.getElementById(\"flights-search-to\").value;";
                String text2 = (String) js.executeScript(script2);
                while (!text2.equalsIgnoreCase("Paris (CDG) Charles de Gaulle, France")) {
                    driver.findElement(By.id(po.arriveat)).sendKeys(Keys.DOWN);

                    text2 = (String) js.executeScript(script2);
                    System.out.println(text2);
                }
                driver.findElement(By.id(po.arriveat)).sendKeys(Keys.TAB);

                break;
            default:
                throw new RuntimeException(/*decider + " NOT FOUND"*/);
        }

    }

    public void surveryHandler() {
        int p = 0;
        try {
            waits.implicitWait();
            p = driver.findElements(By.xpath("//*[@id='acsMainInvite']/a")).size();
            System.out.println("p=" + p);
            driver.findElement(By.xpath("//*[@id='acsMainInvite']/a")).click();
            System.out.println("survey dismissed!");
        }
//   pop up is not present
        catch (Throwable Nopopup) {

        }
    }

    public void getMessage(String type, String element) {
        switch (type) {
            case "Error":
                String ExpectedM = po.ExpectedM;
                String M = driver.findElement(By.xpath(po.Errorlocator)).getText();
                Assert.assertEquals(ExpectedM, M);
                break;

        }


    }


}

