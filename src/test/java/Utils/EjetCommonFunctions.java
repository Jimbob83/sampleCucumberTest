package helperclasses;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class EjetCommonFunctions {

    public WebDriver driver;


    public org.openqa.selenium.WebDriver driver() {
        String info = System.getProperty(info).toLowerCase();
        System.out.println(info);
        if (info.contains("win")) {
            System.setProperty(ChromeDriver, System.getProperty("C:\\Users\\James\\Documents\\Tester\\chromedriver.exe"));
        }
        else if (info.contains("mac")) {
            System.setProperty(ChromeDriver, System.getProperty("/Users/james.millen/IdeaProjects/sampleCucumberTest/src/test/resources/drivers"));
        }
            driver = new ChromeDriver();
        return driver;


    }

    public void goTo(String url) {
        driver().get(url);
    }

    public String dairport() {
        String dairport = "Newcastle";
        return dairport;
    }

    public void departure() {
        driver.findElement(By.id("departureRoundtrip0")).sendKeys(dairport());
    }

    public String arrairport() {
        String arrairport = "Costa Brava";
        return arrairport;
    }

    public void arrival() {
        driver.findElement(By.id("arrivalRoundtrip0")).sendKeys(arrairport());
    }

    public void outdate() {
        //find calendar widget & click
        driver.findElement(By.id("departureDateRoundtrip0")).click();
        //cycle through month until text contains desired month
        while (!driver.findElement(By.className("ui-datepicker-title")).getText().contains("December")) {
            driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/a[2]")).click();
        }

        //all calendar days share samee classnmae so create a list of everything with that classname
        String test = driver.findElement(By.className("ui-state-default")).getText();
        // System.out.println(test);
        List<WebElement> dates = driver.findElements(By.className("ui-state-default"));

//iterate through all the classnames in list until one contains desired number
        int count = driver.findElements(By.className("ui-state-default")).size();
        for (int i = 0; i < count; i++) {
            String text = driver.findElements(By.className("ui-state-default")).get(i).getText();
            if (text.equalsIgnoreCase("5")) {
                driver.findElements(By.className("ui-state-default")).get(i).click();
                break;
            }
        }
    }

    public void returndate() {
        //find calendar widget & click
        driver.findElement(By.id("departureDateRoundtrip1")).click();
        //cycle through month until text contains desired month
        while (!driver.findElement(By.className("ui-datepicker-title")).getText().contains("May")) {
            driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/a[2]")).click();
        }

        //all calendar days share samee classnmae so create a list of everything with that classname
        String test = driver.findElement(By.className("ui-state-default")).getText();
        // System.out.println(test);
        List<WebElement> dates = driver.findElements(By.className("ui-state-default"));

//iterate through all the classnames in list until one contains desired number
        int count = driver.findElements(By.className("ui-state-default")).size();
        for (int i = 0; i < count; i++) {
            String text = driver.findElements(By.className("ui-state-default")).get(i).getText();
            if (text.equalsIgnoreCase("15")) {
                driver.findElements(By.className("ui-state-default")).get(i).click();
                break;
            }

        }
    }
public void search()
{driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    driver.findElement(By.className("trip-search")).click();

}
public void results() throws InterruptedException {Thread.sleep(15000);
    driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
    WebDriverWait wait = new WebDriverWait(driver, 20);

    String result = driver.findElement(By.id("search-not-found")).getText();
    System.out.println(result);

    Assert.assertEquals("We've searched for flights for all airports in related cities Newcastle - Barcelona\n" +
            "Choose the best flight for you.", result);

}
public void ebaysearch() throws InterruptedException {
        Thread.sleep(5000);
        driver.findElement(By.id("gh-ac")).sendKeys("camera");
        //driver.findElement(By.xpath("//*[@id='gh-ac-box']")).sendKeys("camera");
        driver.findElement(By.id("gh-btn")).click();
}



}
