@wip
Feature: search for availability
  Scenario: User searches for travel
    Given I am on "https://www.lastminute.com"
    And I click "Flights"
    When I enter "Newc" in "departure"
   And I enter "Par" in "arrival"
    And I select "18" "December 2018" as "outbounddate"
    And I select "19" "January 2019" as "returndate"
    And I select "Search"
   Then i should be presented with "results"